import React from 'react';

const Overview = (props) => {
  const { tasks, onDeleteTask, onEditTask } = props;

  return (
    <ul className="list-group">
      {tasks.map((task) => {
        return (
          <div>
            <li
              key={task.key}
              id={task.key}
              className="list-group-item d-flex justify-content-around align-items-center"
            >
              <div className="mr-auto"> {task.task}</div>
              <button className="btn btn-success btn-sm" onClick={onEditTask}>
                edit
              </button>
              <button className="btn btn-danger btn-sm" onClick={onDeleteTask}>
                remove
              </button>
            </li>
          </div>
        );
      })}
    </ul>
  );
};

export default Overview;
