import React, { Component } from 'react';
import Overview from './components/Overview';
import uniqid from 'uniqid';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      task: '',
      tasks: [],
    };
  }

  handleChange = (e) => {
    this.setState({
      task: e.target.value,
    });
  };

  onSubmitTask = (e) => {
    e.preventDefault();
    const updateTasks = [...this.state.tasks];
    const taskId = document.querySelector('#taskInput');
    if (taskId.getAttribute('taskid') != '') {
      updateTasks.forEach((task) => {
        if (task.key === taskId.getAttribute('taskid')) {
          task.task = this.state.task;
        }
      });
      //updateTasks.push({ key: taskId, task: this.state.task }); // Array.push() returns the number of elements in arary.
    } else {
      updateTasks.push({ key: uniqid(), task: this.state.task });
    }

    this.setState({
      tasks: updateTasks,
      task: '',
    });

    taskId.setAttribute('taskid', '');
  };

  onDeleteTask = (e) => {
    e.preventDefault();
    this.setState({
      tasks: this.state.tasks.filter(
        (task) => task.key !== e.target.parentElement.getAttribute('id')
      ),
    });
  };

  onEditTask = (e) => {
    e.preventDefault();
    this.setState({
      task: e.target.parentElement.firstChild.textContent,
    });

    document
      .querySelector('#taskInput')
      .setAttribute('taskid', e.target.parentElement.getAttribute('id'));
  };

  render() {
    const { task, tasks } = this.state;

    return (
      <div className="col-6 mx-auto mt-5">
        <form onSubmit={this.onSubmitTask}>
          <div className="form-group">
            <label htmlFor="taskInput">Enter task</label>
            <input
              type="text"
              id="taskInput"
              taskid=""
              className="form-control"
              onChange={this.handleChange}
              value={this.state.task}
            />
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-primary">
              Add Task
            </button>
          </div>
        </form>
        <Overview
          tasks={this.state.tasks}
          onDeleteTask={this.onDeleteTask}
          onEditTask={this.onEditTask}
        />
      </div>
    );
  }
}

export default App;
